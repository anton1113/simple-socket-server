package com.arash.edu.sss.http;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HttpResponse {

    private HttpStatus httpStatus;
    private String body;
    private String dateTime;

    public HttpResponse(HttpStatus httpStatus, String body) {
        this.httpStatus = httpStatus;
        this.body = body;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        this.dateTime = simpleDateFormat.format(new Date());
    }

    public String toString() {
        return String.format("HTTP/2.0 %d %s\n" +
                "date: %s\n" +
                "expires: -1\n" +
                "cache-control: private, max-age=0\n" +
                "content-type: text/html; charset=UTF-8\n" +
                "\n" +
                "%s\n", httpStatus.getCode(), httpStatus.getText(), dateTime, body);
    }
}
