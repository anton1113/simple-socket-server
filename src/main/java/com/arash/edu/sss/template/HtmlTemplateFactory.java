package com.arash.edu.sss.template;

import com.arash.edu.sss.io.utils.IOUtils;

import java.io.InputStream;

public class HtmlTemplateFactory {

    public String getTemplate() {
        InputStream is = getClass().getResourceAsStream("/templates/404.html");
        return IOUtils.readInputStreamAsString(is);
    }
}
