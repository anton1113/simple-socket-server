package com.arash.edu.sss.io;

import com.arash.edu.sss.io.utils.IOUtils;

import java.io.IOException;
import java.net.Socket;

public class SocketReader {

    public String readSocketAsString(Socket socket) throws IOException {
        return new String(readSocketAsByteArray(socket));
    }

    public byte[] readSocketAsByteArray(Socket socket) throws IOException {
        return IOUtils.readInputStreamAsByteArray(socket.getInputStream());
    }
}
