package com.arash.edu.sss.io.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class IOUtils {

    private IOUtils() {}

    public static String readInputStreamAsString(InputStream inputStream) {
        byte[] content = readInputStreamAsByteArray(inputStream);
        return content != null ? new String(content) : null;
    }

    public static byte[] readInputStreamAsByteArray(InputStream inputStream) {
        try {
            byte[] content = new byte[inputStream.available()];
            inputStream.read(content);
            return content;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void writeToOutputStream(String output, OutputStream outputStream) {
        writeToOutputStream(output.getBytes(), outputStream);
    }

    public static void writeToOutputStream(byte[] output, OutputStream outputStream) {
        try {
            outputStream.write(output);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException("Unable to write to OutputStream");
        }
    }
}
