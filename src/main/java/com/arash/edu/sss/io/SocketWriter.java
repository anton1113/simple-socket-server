package com.arash.edu.sss.io;

import com.arash.edu.sss.io.utils.IOUtils;

import java.io.IOException;
import java.net.Socket;

public class SocketWriter {

    public void write(String output, Socket socket) throws IOException {
        write(output.getBytes(), socket);
    }

    public void write(byte[] content, Socket socket) throws IOException {
        IOUtils.writeToOutputStream(content, socket.getOutputStream());
    }
}
