package com.arash.edu.sss.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WebServer {

    private ExecutorService pool;
    private ServerSocket serverSocket;
    private boolean stopped;

    public WebServer(int port) throws IOException {
        init(port, Executors.newCachedThreadPool());
        startServer();
    }

    private void startServer() {
        while (!stopped) {
            try {
                Socket connection = serverSocket.accept();
                Runnable connectionTask = new ConnectionTask(connection);
                pool.submit(connectionTask);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void init(int port, ExecutorService threadPool) throws IOException {
        serverSocket = new ServerSocket(port);
        pool = threadPool;
    }
}
