package com.arash.edu.sss.server;

import com.arash.edu.sss.http.HttpRequest;
import com.arash.edu.sss.http.HttpResponse;
import com.arash.edu.sss.http.HttpStatus;
import com.arash.edu.sss.io.SocketReader;
import com.arash.edu.sss.io.SocketWriter;
import com.arash.edu.sss.template.HtmlTemplateFactory;

import java.io.IOException;
import java.net.Socket;

public class ConnectionTask implements Runnable {

    private Socket socket;

    public ConnectionTask(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try(Socket connection = socket) {
            SocketReader reader = new SocketReader();
            SocketWriter writer = new SocketWriter();
            String rawRequest = reader.readSocketAsString(connection);
            HttpRequest request = new HttpRequest(rawRequest);
            HtmlTemplateFactory htmlTemplateFactory = new HtmlTemplateFactory();
            String rawResponse = htmlTemplateFactory.getTemplate();
            HttpResponse response = new HttpResponse(HttpStatus.OK, rawResponse);
            writer.write(response.toString(), connection);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
